import {hoverTarget} from "./scripts/hover-target.js";
import {critCheck} from "./scripts/crit-test.js";
import {compareAttacks} from "./scripts/attack-roller.js";
import {forceDCSave} from "./scripts/save-roller.js";
import {AbilityTemplate} from "./scripts/abilityTemplate.js";
import {showResults} from "./scripts/show-results.js";

//Clamp Function, pretty handy:
Math.clamp = function (number, min, max) {
    return Math.max(min, Math.min(number, max));
}

//Save DC Button Creation:
function createDCButton(data) {
     let content = data.content;
     content = content.split('\n');
     for (let index = 0; index < content.length; index++) {
         const element = content[index];
         if (element.includes("Save DC")) {
             content[index] = content[index].replace(
                 `<button data-action="save`, '<button data-action="saveDC');
             content[index] = content[index].replace("</span>", "</button>");
         }
     }
     content = content.join('\n');
     return content;
}

Hooks.on("createChatMessage", (message) => {
    if (game.user._id === game.users.entities.find(u => u.isGM)._id || game.settings.get("pf2qr", "PlayersCanQuickRoll")) {
        if (game.combat === null || (game.user._id === game.users.entities.find(u => u.isGM)._id) || !game.settings.get("pf2qr", "RollOnTurn") || (game.combat.combatant.players[0].data._id === game.user._id)) {
            if (message.data.content.includes('<button>Save DC')) createDCButtonListener(message);

            if (!message.data) return true;
            if (!message.data.flavor) return true;
            if (message.data.flavor.includes('Attack Roll') && message.data.user === game.user.data._id) compareAttacks(message);
            if (message.data.flavor.includes('Strike:') && message.data.user === game.user.data._id) compareAttacks(message);
        }
    }
});

// Handle-socket
Hooks.on('ready', () => {
    console.log("Registering...")
    game.socket.on('module.pf2qr', async (data) => {
        if (game.user.isGM && data) showResults(data);
      });
    })

//Pre-Create:
Hooks.on("preCreateChatMessage", (data) => {
    if (!game.settings.get("pf2qr", "SystemSaving") && data.content.includes(`<button data-action="save`)) data.content = createDCButton(data);
    if (data.content.includes(`Area:`)) data.content = addTemplateButton(data);
});

Hooks.on('ready', () => {
    [
        {
            name: "ShowPlayersResults",
            hint: "Whether players should see the results of rolls. Private/Blind rolls will serve a similar function soon(TM).",
            scope: "world",
            default: true,
            type: Boolean,
        }, {
            name: "ShowExceedsBy",
            hint: "Whether to show how much a roll exceeded the AC/DC for.",
            scope: "world",
            default: true,
            type: Boolean,
        }, {
            name: "PlayersCanQuickRoll",
            hint: "If disabled, only the GM can quick roll.",
            scope: "world",
            default: true,
            type: Boolean,
        }, {
            //     name: "UseSelection",
            //     hint: "If enabled, selected tokens are rolled for instead of targeted tokens. This means players cannot quick roll, overrides PlayersCanQuickRoll.",
            //     scope: "world",
            //     default: false,
            //     type: Boolean,
            // }, {
            //     name: "AskPermissionForRoll",
            //     hint: "Players can quick roll, however the GM is sent a request for permission each time.",
            //     scope: "world",
            //     default: false,
            //     type: Boolean,
            // }, {
            name: "RollOnTurn",
            hint: "Players can only roll on their turn in combat. Can roll whenever if not in combat.",
            scope: "world",
            default: false,
            type: Boolean,
        }, {
             name: "StopTargetingAfterSave",
             hint: "After quick saves are rolled, releases target on tokens.",
             scope: "world",
             default: false,
             type: Boolean
        }, {
            name: "ShowBubbles",
            hint: "Shows bubbles above the heads of tokens for their success. Keep in mind this shows the results to players, though not the exceed by value.",
            scope: "world",
            default: true,
            type: Boolean,

        }, {
            name: "SystemSaving",
            hint: "Use the default pathfinder saving instead of PF2QR target saving.",
            scope: "world",
            default: false,
            type: Boolean
        }
    ].forEach((setting) => {
        let options = {
            name: setting.name,
            hint: setting.hint,
            scope: setting.scope,
            config: true,
            default: setting.default,
            type: setting.type,
        };
        game.settings.register("pf2qr", setting.name, options);
    });

});

Hooks.on('renderChatLog', (log, html) => {
    
    hoverTarget();

    html.on('click', '.card-buttons button', (ev) => {
        let cardID = $(ev)[0].originalEvent.path[2].dataset.itemId
        const button = $(ev.currentTarget);
        const dc = ev.currentTarget.innerText.match(/\d+/g)
        const action = button.attr('data-action');
        const actionSave = button.attr('data-save')
        if (action.includes('saveDC')) forceDCSave(actionSave, dc, cardID);
        if (action.includes('template')) createTemplate(button);
    });
});

//Creating of the template buttons in chat log:
function addTemplateButton(data) {
    let content = data.content;
    content = content.split('\n');

    let range = 0;
    let shape = "ERROR";
	
	let templates = {};

    for (let index = 0; index < content.length; index++) {
        const element = content[index];
        if (element.includes("Area:")) {
			let matchGroups = element.match(/(\d+).foot.(\w+)/ig);
			matchGroups.forEach(group =>{
				let data = group.match(/(\d+).foot.(\w+)/i);
				range = data[1];
				switch(data[2].toLowerCase()){
					case 'burst':
						shape='circle';
						break;
					case 'cone':
						shape="cone";
						break;
					case 'emanation':
						shape='rect';
						break;
					case 'line':
						shape='ray';
						break;
				}
				templates["<div class='card-buttons'><button data-action='template' class='templateButton' data-area='"+range+"' data-shape='"+shape+"'>Place a "+range+"ft "+shape+"</button></div>"] = 0;
			})
        }
    }
    content = content.join('\n');
	for(const template in templates){
		content += '\n' + template
	}
	return content;
}

var pf2qrGhostTemplate = null;

function createTemplate(button){
    const range = (button[0].attributes['data-area'].value);
    const type = (button[0].attributes['data-shape'].value);
    console.log(type);

    if(pf2qrGhostTemplate!=null) pf2qrGhostTemplate.kill();
    const templateData = {
        t: type,
        user: game.user._id,
        x: 1000,
        y: 1000,
        direction: 180,
        angle: 90,
        distance: range,
        borderColor: "#FF0000",
        fillColor: "#FF3366",
    };
    if(type==="ray"){
        templateData.width=5;
    }
    if(type==="rect"){
        templateData.direction=45;
        templateData.width=range;
        templateData.distance=Math.hypot(range,range);
    }

    pf2qrGhostTemplate = new AbilityTemplate(templateData);
    pf2qrGhostTemplate.drawPreview();
}
