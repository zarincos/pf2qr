import {critCheck} from "./crit-test.js";
import {showResults} from "./show-results.js";

export function compareAttacks(message) {
    let compiledMessage = `<div><h3 style='border-bottom: 3px solid black'>Attacks:</h3></div>`;
    game.user.targets.forEach(t => {

        //Success step meaning:
        // 3 = Critical
        // 2 = success
        // 1 = failure
        // 0 = critical failure
        let successStep = -1;

        //getting the base level of success from the roll:
        if (message.roll.total >= t.actor.data.data.attributes.ac.value) {
            successStep = 2;
        } else {
            successStep = 1;
        }

        //Augmenting the success by criticals and natural 20s/1s:
        successStep += critCheck(message.roll, t.actor.data.data.attributes.ac.value);

        //Ensuring the successStep doesn't somehow break the system catastrophically?
        successStep = Math.clamp(successStep, 0, 3);

        let successBy = message.roll.total - t.actor.data.data.attributes.ac.value;
        switch (successStep) {
            case 0:
                //REIMPLEMENT WITH SETTINGS: t.setTarget(false, { releaseOthers: false, grouSelection: true });
                compiledMessage +=
                    `<div class="targetPicker" data-target="${t.data._id}" data-hitType="cm">
                <div style="color:#131516;margin-top:4px;">
                💔 <b>${t.name}:</b>  
                </div>
                <div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
                💔 
                <b style="color:#990000">
                Critically missed${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}!` : `!`)}
                </b>
                </div>
                </div>`
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "💔 Crit Fail", { emote: true });
                break;
            case 1:
                //REIMPLEMENT WITH SETTINGS: t.setTarget(false, { releaseOthers: false, grouSelection: true });
                compiledMessage +=
                    `<div class="targetPicker" data-target="${t.data._id}" data-hitType="m">
                <div style="color:#131516;margin-top:4px;">
                ❌ <b>${t.name}:</b> 
                </div>
                <div style="color:#131516;border-bottom: 2px solid black;padding-bottom:4px;">
                ❌ Missed${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}
                </div>
                </div>`
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "❌ Fail", { emote: true });
                break;
            case 2:
                compiledMessage +=
                    `<div class="targetPicker" data-target="${t.data._id}" data-hitType="h">
                <div style="color:#131516;margin-top:4px;">
                <b style="color:#4C7D4C">✔️</b> <b>${t.name}:</b> 
                </div>
                <div style="color:#131516;border-bottom: 2px solid black;padding-bottom:4px;">
                <b style="color:#4C7D4C">✔️</b> Hit${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}
                </div>
                </div>`
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, `<b style="color:#4C7D4C">✔️</b> Success`, { emote: true });
                break;
            case 3:
                compiledMessage += //`<div style="color:#131516;  border-bottom: 1px solid black;">${t.name}:   💥 <b style="color:#4C7D4C">Critical Success</b> ${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}</div>`
                    `<div class="targetPicker" data-target="${t.data._id}" data-hitType="ch">
                <div style="color:#131516;margin-top:4px;">
                💥 <b>${t.name}:</b>   
                </div>
                <div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
                💥 
                <b style="color:#4C7D4C">
                Critically Hit${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}!` : `!`)}
                </b>
                </div>
                </div>`
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "💥 Crit Success", { emote: true });
                break;
            default:
                compiledMessage += `<div style="color:#FF0000">error. Try again, if that doesn't work, your roll command is invalid. Please report it to @Darth_Apples#4725 or on the gitlab page.</div>`;
                break;
        }
    });
    //Determining permissions, and whether to show result or not:
    if (game.user.targets.size > 0) {
        let chatData = {
            user: game.users.entities.find(u => u.isGM)._id,
            content: compiledMessage
        }
        showResults(chatData);
    }
}
