export function showResults(chatData) {
    //Use this to determine if it should be shown to players or not.
    if (!game.settings.get("pf2qr", "ShowPlayersResults")) {
        chatData.user = game.users.entities.find(u => u.isGM)._id; //Imitating GM so that we don't see our own message to the GM, in the case it is a player rolling.
        chatData.speaker = ChatMessage.getSpeaker({ user: game.user });
    } 

// Socket decisions. If user is not a Gm and showresults is true, send data to GM.

// If GM and showResults is true, process chatmessage as normal and whisper to GM

// Else if setting is still false, create chatdata publicly
    if (!game.user.isGM && !game.settings.get("pf2qr", "ShowPlayersResults"))
    {
        console.log("EMITTING USER DATA TO GM")
        game.socket.emit('module.pf2qr', chatData);

    } else if (game.user.isGM && !game.settings.get("pf2qr", "ShowPlayersResults")) {

        chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
        
        console.log("PROCESSING USER DATA AS GM!")
        ChatMessage.create(chatData);
        
    } else if (game.settings.get("pf2qr", "ShowPlayersResults"))
    {
        ChatMessage.create(chatData);
    }
}
