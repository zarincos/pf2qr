import {critCheck} from "./crit-test.js";
import {showResults} from "./show-results.js";

let messages = []
let messageCount;

const StatusTextsByStep = {
	0: { "hitType": "cm", "symbol": "💔", "color": "#990000", "message": "Critically failed", "chatMessage": "💔 Crit Fail" },
	1: { "hitType": "m", "symbol": "❌", "color": "#131516", "message": "Failed", "chatMessage": "❌ Fail" },
	2: { "hitType": "h", "symbol": "✔️", "color": "#131516", "message": "Succeeded", "chatMessage": "✔️ Success" },
	3: { "hitType": "ch", "symbol": "💥", "color": "#4C7D4C", "message": "Critically succeeded", "chatMessage": "💥 Crit Success" }
};

export async function forceDCSave(action, dc, id) {

	messages = [];
	messageCount = [...game.user.targets].length;

   let saveName = 'error';

    if (action.includes('reflex')) {
        saveName = 'reflex';
    }
    if (action.includes('will')) {
        saveName = 'will';
    }
    if (action.includes('fortitude')) {
        saveName = 'fortitude';
    }
    if (saveName === 'error') {
        ui.notifications.error("Invalid save type. Make sure this particular spell has a save type in its config, not all start with one.")
        return;
    }

    let tokens = canvas.tokens.controlled[0];

    if (!tokens){
        ui.notifications.error("You have not selected your token. Please select the caster")
        return;
    }

    if (!tokens.actor.data.items.find(item => item._id === id)){
    ui.notifications.error("The spell was not found on the selected token. Please select the caster of the spell.")
        return;
    }
    
    let spellTraits = tokens.actor.data.items.find(item => item._id === id).data.traits.value

    dc = parseInt(dc[0]);
    
	[...game.user.targets].map(token => {
		rollTokenSave(token, action, dc, spellTraits);
	});
}

function rollTokenSave(token, action, dc, spellTraits) {
	token.actor.data.data.saves[action].roll(event, spellTraits, (result) => {
		//Success step meaning:
		// 3 = Critical
		// 2 = success
		// 1 = failure
		// 0 = critical failure
		let successStep = -1;

		//Rolling the save from the target actor. WILL NOT WORK OF QUICKROLLS ARE NOT ENABLED

		let roll = result

		if (!roll) {
			return '';
		}

		//Setting the basic roll from its comparison with DC:
		if (roll._total >= dc) {
			successStep = 2;
		} else {
			successStep = 1;
		}

		//applying effects of crits and naturals:
		successStep += critCheck(roll, dc);
		successStep = Math.clamp(successStep, 0, 3);

		let successBy = roll._total - dc;

		if (successStep < 0 || successStep > 3) {
			sendMessage(`<div style="color:#FF0000">error. Try again, if that doesn't work, your roll command is invalid. Please report it to @Darth_Apples#4725</div>`);
		}

		sendMessage(createMessage(token, successStep, successBy));

		if (game.settings.get("pf2qr", "StopTargetingAfterSave")) token.setTarget(false, { releaseOthers: false, grouSelection: true });
	});
}

function sendMessage(message) {
	messages.push(message);
	if (messages.length != messageCount) {
		return;
	}
	//finishing message:
	let compiledMessage = "<div><h3 style='border-bottom: 3px solid black'>Save Results"/* DC" + dc +*/ + ":</h3>" //ADD A CONFIG FOR THIS IN FUTURE PLEASE!
	compiledMessage += messages.join('');
	compiledMessage += "</div>"

	let chatData = {
		user: game.users.entities.find(u => u.isGM)._id,
		content: compiledMessage	
	}
	showResults(chatData);
}

function createMessage(token, successStep, successBy) {
	let status = StatusTextsByStep[successStep];
	if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(token, status.chatMessage, { emote: true });
	
	return `
			<div class="targetPicker" data-target="${token.data._id}" data-hitType="${status.hitType}">
				<div style="color:#131516;margin-top:4px;">
					${status.symbol} <b>${token.name}:</b>
				</div>
				<div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
					${status.symbol}
				<b style="color:${status.color}">
					${status.message}${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}` : ``)}${successStep == 0 || successStep == 3 ? '!' : '.'}
				</b>
				</div>
			</div>`;
}
